## Install Project
Don't forget to edit .env and provide your database details firest before running php artisan migrate. Run the following command to fully initiate the project.

```
composer install
cp .env.example .env
php artisan key:generate
npm install
php artisan migrate:fresh --seed
```

This package has basic roles and permissions, users, employees and related information.

Here is the superadmin detail: 
````
email: super@admin.com
password: 123
````
## Have fun developing
