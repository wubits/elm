<?php

namespace App\Contracts\Repositories;

use Spatie\Permission\Models\Role;

interface RoleRepositoryInterface
{
    public function latest();

    public function one($roleId);

    public function create($data);

    public function update($roleId, $data);

    public function delete($roleId);

    public function allPermissions();

    public function permissionIDsArrayByRole($roleId);
}
