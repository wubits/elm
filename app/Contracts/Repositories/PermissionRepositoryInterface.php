<?php

namespace App\Contracts\Repositories;

interface PermissionRepositoryInterface
{
    public function latest();

    public function one($id);

    public function idArrayByRoleId($id);
}
