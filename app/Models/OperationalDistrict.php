<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Province;
use App\Models\HealthFacility;

class OperationalDistrict extends Model
{
    use HasFactory;

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function healthFacilities(){
        return $this->hasMany(HealthFacility::class);
    }

    public function getFullnameAttribute(){
        return $this->province->name . ' - ' . __('mission.operational_district') . ' ' . $this->name;
    }
}
