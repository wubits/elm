<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Position;
use App\Models\EmployeeMission;

class Employee extends Model
{
    private $name;
    protected $fillable=['fullname','sex','position_id','notes'];

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function getNameAttribute(){
        return $this->title . ' ' . $this->fullname . ' ( ' . $this->position->name . ' ' . $this->position->department->name . ' )';
    }

}
