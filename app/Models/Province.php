<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\OperationalDistrict;

class Province extends Model
{
    use HasFactory;

    public function operationalDistricts()
    {
        return $this->hasMany(OperationalDistrict::class);
    }
}
