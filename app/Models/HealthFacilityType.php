<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\HealthFacility;

class HealthFacilityType extends Model
{
    use HasFactory;

    public function healthFacilities(){
        return $this->hasMany(HealthFacility::class);
    }

}
