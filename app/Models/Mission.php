<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use KhmerDateTime\KhmerDateTime;
use Illuminate\Database\Eloquent\Model;
use App\Models\Province;
use App\Models\OperationalDistrict;
use App\Models\HealthFacility;
use App\Models\Objective;
use App\Models\Transportation;
use App\Models\EmployeeMission;
use App\Models\Destination;
use App\Models\Missionable;
use Illuminate\Support\Facades\Auth;

class Mission extends BaseModel
{
    use HasFactory;

    protected $dates = ['leave_date', 'return_date'];

    protected $fillable = ['code', 'reference_document','notes', 'leave_date', 'return_date',
        'objective_id', 'transportation_id', 'budget_id', 'status_id', 'updated_by'];


    public function provinces()
    {
        return $this->morphedByMany(Province::class, 'missionable');
    }

    public function operationalDistricts()
    {
        return $this->morphedByMany(OperationalDistrict::class, 'missionable');
    }

    public function healthFacilities()
    {
        return $this->morphedByMany(HealthFacility::class, 'missionable');
    }

    public function objective()
    {
        return $this->belongsTo(Objective::class);
    }

    public function transportation()
    {
        return $this->belongsTo(Transportation::class);
    }

    public function members()
    {
        return $this->belongsToMany(Employee::class)
            ->withPivot('mission_role_id')
            ->using(MissionRolePivot::class);
    }

    public function missioned(){
        return $this->hasMany(Missionable::class);
    }

    public function getDestinationsAttribute(){
        return array_merge(
            $this->provinces->pluck('name')->toArray(),
            $this->operationalDistricts->pluck('fullname')->toArray(),
            $this->healthFacilities->pluck('completename')->toArray());
    }

    public function getKhLeaveDateAttribute(){
        $dateTime = KhmerDateTime::parse($this->leave_date);
        return $dateTime->format("LL");
    }

    public function getKhReturnDateAttribute(){
        $dateTime = KhmerDateTime::parse($this->return_date);
        return $dateTime->format("LL");
    }

}
