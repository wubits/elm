<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Missionable extends Model
{
    public function missionables(){
        return $this->morphTo();
    }
}
