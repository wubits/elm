<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\HealthFacilityType;
use App\Models\OperationalDistrict;

class HealthFacility extends Model
{
    use HasFactory;

    protected $appends = ['fullname'];

    public function operationalDistrict()
    {
        return $this->belongsTo(OperationalDistrict::class);
    }

    public function healthFacilityType()
    {
        return $this->belongsTo(HealthFacilityType::class);
    }

    public function getFullNameAttribute(){
        //return $this->name . ' - ' . $this->type->name;
        return "{$this->healthFacilityType->name} {$this->name}";
    }

    public function getCompleteNameAttribute(){
        return $this->operationalDistrict->province->name . ' - ' . $this->fullname;
    }
}
