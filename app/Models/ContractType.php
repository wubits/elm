<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Position;

class ContractType extends Model
{
    public function position()
    {
        return $this->hasMany(position::class);
    }
}
