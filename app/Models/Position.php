<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Employee;
// use App\Models\Department;
// use App\Models\ContractType;

class Position extends Model
{
    // use HasFactory;
    
    public function employee()
    {
        return $this->hasMany(employee::class);
    }

    public function department()
    {
        return $this->belongsTo(department::class);
    }

    public function contractType()
    {
        return $this->belongsTo(contractType::class);
    }


}
