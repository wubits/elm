<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class MissionRolePivot extends Pivot
{
    public $incrementing = true;

    public function role(){
        return $this->belongsTo(MissionRole::class, 'mission_role_id', 'id');
    }
}
