<?php

namespace App\Http\Requests\createrequest;

use Illuminate\Foundation\Http\FormRequest;

class MissionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->missionProvinces);
        return [
            //
            'missionProvinces.*' => 'required|Numeric|min:1',
            'missionMember.*' => 'required',
            'missionLeaveDate' => 'required',
            'missionReturnDate' => 'required',
            'missionReason'=>'required|min:1',
            'missionTransmission'=>'required',
            'missionBuget'=>'required',

        ];
    }

    public function messages()
    {
        return [

            'missionProvinces.*.min' => 'អ្នកមិនបានជ្រើសរើសខេត្តអោយបានត្រឹមត្រូវ',
            'missionMember.*.required' => 'អ្នកមិនបានជ្រើសរើសសមាជិកអោយបានត្រឹមត្រូវ',
            'missionLeaveDate.required' => 'សូមបំពេញថ្ងៃចេញដំណើរ',
            'missionReturnDate.required' => 'សូមបំពេញថ្ងៃត្រលប់មកវិញ',
            'missionReason.required'=>'សូមជ្រើសរើសគោលបំណងចុះបេសកកម្ម',
            'missionTransmission.required'=>'សូមជ្រើសរើសមធ្យោបាយធ្វើដំណើរ',
            'missionBuget.required'=>'សូមជ្រើសរើសប្រភពថវិកា',

        ];
    }
}
