<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Hash;
use Auth;
use App\Rules\MatchOldPassword;

class MatchOldPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ];
    }

    public function messages()
    {
        return [
            'current_password.required' => __('app.current_password_required'),
            'new_password.required' => __('app.new_password_required'),
            'new_confirm_password.same' => __('app.new_password_not_match'),
        ];
    }
}
