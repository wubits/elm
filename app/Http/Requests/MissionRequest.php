<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provinces.*' => 'required|numeric|min:1',
            'members.*' => 'required|numeric|min:1',
//            'code' => 'required',
            'leave_date' => 'required',
            'return_date' => 'required',
            'objective_id'=>'required|min:1',
            'transportation_id'=>'required',
            'budget_id'=>'required',
            'reference_document' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'provinces.*.min' => __('message.error_mission_pick_one_destination'),
            'provinces.*.required' => __('message.error_mission_add_destination'),
            'members.*.min' => __('message.error_mission_pick_one_member'),
            'leave_date.required' => __('message.error_mission_leave_date_is_required'),
            'return_date.required' => __('message.error_mission_return_date_is_required'),
            'objective_id.required' => __('message.error_mission_objective_is_required'),
            'transportation_id.required' => __('message.error_mission_transportation_is_required'),
            'budget_id.required' => __('message.error_mission_budget_is_required'),
            'reference_document.required' => __('message.error_reference_document_is_required'),
        ];
    }
}
