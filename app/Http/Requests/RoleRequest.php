<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:roles,name,' . Request::instance()->id,
            'permissions' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('role.name_required'),
            'name.unique' => __('role.name_unique'),
            'permissions.required' => __('role.permissions_required'),
        ];
    }
}
