<?php

namespace App\Http\Livewire\Transportation;

use Livewire\Component;
use App\Models\Transportation;

class Form extends Component
{
    public Transportation $transportation;
    public $editing = false;

    protected $rules = [
        'transportation.id' => '',
        'transportation.name' => 'required|unique:transportations,name',
        'transportation.plate_number' => 'required|unique:transportations,plate_number',
    ];

    protected $listeners= ['clear'];

    public function mount(){
        $this->transportation = new Transportation();
    }

    public function render()
    {
        return view('livewire.transportation.form');
    }

    public function save()
    {
        $this->validate();
        $this->transportation->save();
        $this->emit('saved', $this->transportation->id);
    }

    public function clear(){
        $this->transportation = new Transportation();
        $this->editing = false;
    }
}
