<?php

namespace App\Http\Livewire\Transportation;

use App\Models\Transportation;
use Livewire\Component;

class SelectBox extends Component
{
    public $transportations;
    public $transportation;
    protected $listeners = ['saved' => 'select'];

    public function render()
    {
        $this->transportations = Transportation::all();
        return view('livewire.transportation.select-box');
    }

    public function select($selected){
        $this->transportation = $selected;
        $this->render();
        $this->emit('clear');
    }
}
