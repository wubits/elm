<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\PermissionRepositoryInterface;
use App\Contracts\Repositories\RoleRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
class RoleController extends Controller
{
    /**
     * @var RoleRepositoryInterface
     */
    private $roleRepository;

    function __construct(RoleRepositoryInterface $roleRepositoryInterface)
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-edit', ['only' => ['create','store', 'edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
        $this->roleRepository = $roleRepositoryInterface;
    }
    public function index()
    {
        $roles = $this->roleRepository->latest();
        return view('roles.index',compact('roles'));
    }
    public function create()
    {
        $permissions = $this->roleRepository->allPermissions();
        return view('roles.create',compact('permissions'));
    }
    public function store(RoleRequest $request)
    {
        $data = $request->only('name', 'permissions');
        $role = $this->roleRepository->create($data);
        return redirect()->route('roles.index')
                        ->with('success', __('role.role') . ' ' . __('app.saved'));
    }
    public function show($id)
    {
        $role = $this->roleRepository->one($id);
        $rolePermissions = $role->permissions;
        return view('roles.show',compact('role','rolePermissions'));
    }

    public function edit($id)
    {
        $role = $this->roleRepository->one($id);
        $permissions = $this->roleRepository->allPermissions();
        $rolePermissions = $this->roleRepository->permissionIDsArrayByRole($id);
        return view('roles.edit',compact('role','permissions','rolePermissions'));
    }

    public function update(RoleRequest $request, $id)
    {
        $data = $request->only('name', 'permissions');
        $this->roleRepository->update($id,  $data );
        return redirect()->route('roles.index')
                        ->with('success', __('role.role') . ' ' . __('app.updated'));
    }
    public function destroy($id)
    {
        $this->roleRepository->delete($id);
        return redirect()->route('roles.index')
                        ->with('success', __('role.role') . ' ' . __('app.deleted'));
    }

}
