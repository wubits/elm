<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Position;
use App\Models\Employee;
use App\Models\Department;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Hash;
class EmployeeController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:employee-list|employee-create|employee-edit|employees-delete', ['only' => ['index','store']]);
        $this->middleware('permission:employee-create', ['only' => ['create','store']]);
        $this->middleware('permission:employee-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:employee-delete', ['only' => ['destroy']]);

    }
    public function index()
    {
        $employees = Employee::all();
        return view('employees.index', compact('employees'));
    }
    public function create()
    {
        $employees = Employee::get();
        $departments = Department::all();
        return view('employees.create',compact('employees','departments'));
    }
    public function store(Request $request){
        // dd($request);
        $this->validate($request, [
            'fullname' => 'required',
            'sex' => 'required',
            'position_id' => 'required',
            'notes' => '',
        ]);
        Employee::create($request->all());
        return redirect()->route('employees.index')
                         ->with('success', __('employee.employee').__('message.success_saved'));
    }
    public function edit($id)
    {
        $employee = Employee::find($id);
        $departments = Department::all();
        $positions = $employee->position->department->positions;
        return view('employees.edit',compact('employee','departments','positions'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fullname' => 'required',
            'sex' => 'required',
            'position_id' => 'required',
            'notes' => '',
        ]);
        $employee = Employee::find($id);
        $employee ->update($request->all());
        return redirect()->route('employees.index')
            ->with('success', __('employee.employee').__('message.success_updated'));

    }
    public function destroy($id)
    {
        DB::table("employees")->where('id',$id)->delete();
        return redirect()->route('employees.index')
                        ->with('success',__('employee.employee').__('message.success_deleted'));
    }
}
