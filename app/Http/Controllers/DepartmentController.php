<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Department;

class DepartmentController extends Controller
{
    //
        /**

     * Get Ajax Request and restun Data

     *

     * @return \Illuminate\Http\Response

     */

    public function positions($depId)

    {
        if($depId > 0){
            $department = Department::find($depId);
            $positions = $department->positions->pluck("name","id");
            return json_encode($positions);
        }
        else{
            return json_encode([]);
        }

    }
}
