<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Arr;
use DB;
use Hash;
use Auth;
use App\Http\Requests\MatchOldPasswordRequest;

class UserController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','store']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }
    public function create()
    {
        $roles = Role::all();
        return view('users.create',compact('roles'));
    }
    public function store(Request $request)
    {
        $this-> validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|same:confirm-password',
        ]);
        $input = $request->all();
        $input['password']= Hash::make($input['password']);

        $user = User::create($input);
        $user-> assignRole($request->input('roles'));
        return redirect()->route('users.index')
            ->with('success', __('app.saved'));
    }
    public function show($id)
    {
        $users = User::find($id);
        return view('users.show',compact('users'));
        //compact('users') ត្រូវយកទៅដាក់ក្នុង Page: index  {{ $users->email }}
    }
    public function edit($id)
    {
        $user = User::find($id);
        // $roles ('name','name') ជាឈ្មោះយកពី database
        $roles = Role::all();
        $userRoles = $user->roles->pluck('id')->toArray();
        return view('users.edit',compact('user','roles','userRoles'));
    }
    public function update(Request $request, $id)
    {
        $this-> validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
        $input = $request->all();
            if(!empty($input['password'])){
                $input['password'] = Hash::make($input['password']);
            }else{
                $input = array_except($input,array('password'));
            }
            $user = User::find($id);
            $user->update($input);
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $user->assignRole($request->input('roles'));
            return redirect()->route('users.index')
                        ->with('success', __('app.updated'));
    }
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('users.index')
                        ->with('success',__('app.deleted'));
    }



}
