<?php

namespace App\Http\Controllers;
use App\Http\Requests\MissionRequest;
use Auth;
use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Mission;
use App\Models\OperationalDistrict;
use App\Models\HealthFacility;
use App\Models\Employee;
use App\Models\Position;
use App\Models\Department;
use App\Models\ContractType;
use App\Models\MissionRole;
use App\Models\Budget;
use App\Models\Objective;
use App\Models\Transportation;
use App\Models\EmployeeMission;
use DB;
use App\Http\Requests\createrequest\MissionCreateRequest;
use Illuminate\View\View;



class MissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    function __construct()
    {
         $this->middleware('permission:mission-detail',['only' => ['show']]);
         $this->middleware('permission:mission-create', ['only' => ['create','store']]);
         $this->middleware('permission:mission-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:mission-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
            $missions = Mission::with('members','provinces','operationalDistricts.province','healthFacilities.operationalDistrict.province','healthFacilities.healthFacilityType')
                ->where("status_id", "=", 1)
                ->orderBy('id', 'DESC')
                ->get();

            //dd($missions);
            return view('missions.index',['missions'=>$missions]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $employees = Employee::with(['position'],['department'])->get();
        $objectives = Objective::select('name','id')->get();
        $budgets = Budget::select('name','id')->get();
        $roles = MissionRole::select('name','id')->get();
        $transportations = Transportation::select('name','id','plate_number')->get();
        $provinces = Province::get();

        return view('missions.create',[
            'provinces' => $provinces,
            'employees'=>$employees,
            'roles' => $roles,
            'budgets'=>$budgets,
            'objectives'=>$objectives,
            'transportations'=>$transportations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(MissionRequest $request)
    {
        $transportation = $request->all();
        foreach($request->health_facilities as $index => $hf) {
            if ($request->health_facilities[$index] > 0){
                unset($transportation['operational_districts'][$index]);
                unset($transportation['provinces'][$index]);
            }
        }
        foreach($transportation['operational_districts'] as $index => $od) {
            if ($transportation['operational_districts'][$index] > 0){
                unset($transportation['provinces'][$index]);
            }
        }

        $mission = Mission::create($request->all());
        foreach($request->members as $index => $member){
            $mission->members()->attach($member, ['mission_role_id' => $request->roles[$index]]);
        }

        $mission->provinces()->attach($transportation['provinces']);
        $mission->operationalDistricts()->attach(array_filter($transportation['operational_districts']));
        $mission->healthFacilities()->attach(array_filter($transportation['health_facilities']));

        return redirect()->route('mission.index')->with('success',__('message.success_mission_saved'));

    }

    /**
     * Display the specified resource.
     * @param  int  $id
     */
    public function show($id)
    {
        $mission = Mission::with(
            'members',
            'objective',
            'healthFacilities',
            'operationalDistricts',
            'provinces'
        )->findOrFail($id);

        return view('missions.show', compact('mission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $mission = Mission::with(
            'members',
            'healthFacilities',
            'operationalDistricts',
            'provinces'
        )->findOrFail($id);

        $employees = Employee::with(['position'],['department'])->get();
        $objectives = Objective::select('name','id')->get();
        $budgets = Budget::select('name','id')->get();
        $roles = MissionRole::select('name','id')->get();
        $transportations = Transportation::select('name','id','plate_number')->get();
        $provinces = Province::get();

        //dd($mission->reference_document);

        return view('missions.edit',[
            'mission' => $mission,
            'provinces' => $provinces,
            'employees'=>$employees,
            'roles' => $roles,
            'budgets'=>$budgets,
            'objectives'=>$objectives,
            'transportations'=>$transportations,
            'province_count' => $mission->provinces->count(),
            'od_count' => $mission->operationalDistricts->count() + $mission->provinces->count(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  MissionRequest  $request
     * @param  int  $id
     */
    public function update(MissionRequest $request, $id)
    {
        $requests = collect($request)->merge([
            'updated_by' => \Illuminate\Support\Facades\Auth::id(),
        ]);

        $transportation = $request->all();
        foreach($request->health_facilities as $index => $hf) {
            if ($request->health_facilities[$index] > 0){
               unset($transportation['operational_districts'][$index]);
               unset($transportation['provinces'][$index]);
            }
        }
        foreach($transportation['operational_districts'] as $index => $od) {
            if ($transportation['operational_districts'][$index] > 0){
                unset($transportation['provinces'][$index]);
            }
        }

        $mission =  Mission::find($id);
        $mission->update($requests->all());

        $mission->members()->detach();
        foreach($request->members as $index => $member){
            $mission->members()->attach($member, ['mission_role_id' => $request->roles[$index]]);
        }
        $mission->provinces()->sync($transportation['provinces']);
        $mission->operationalDistricts()->sync($transportation['operational_districts']);
        $mission->healthFacilities()->sync($transportation['health_facilities']);

        return redirect()->route('mission.index')->with('success',__('message.success_mission_updated'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $delMission = Mission::find($id);
        $delMission->status_id = '0';
        $delMission->updated_by = Auth::user()->id;
        $delMission->save();
        return redirect()->route('mission.index')->with('success',__('message.success_mission_deleted'));

    }

    function getOdsByProId(Request $request){
        $proId = $request->proId;
        $ods = Province::find($proId)->operationalDistricts;
        return json_encode($ods);
    }

    function getFacByProId(Request $request){
        $OpsId = $request->OpsId;
        $hfs = OperationalDistrict::find($OpsId)->healthFacilities;
        return json_encode(collect($hfs));
    }

    function getPositionID(Request $request){

        $positionId = $request->positionId;
        $position = Employee::with(['position'],['department'])->find($positionId);
        return json_encode($position);
    }

    public function rebackDestroy($id)
    {
        $missionID = Mission::find($id);
        $missionID->status_id = '1';
        $missionID->updated_by = Auth::user()->id;
        $missionID->save();
        return back()->with('rebackSuccess','បេសកកម្មលេខ PCAMS'. $missionID->id .' ត្រូវបានត្រលប់វិញ');
    }

}
