<?php


namespace App\Helpers;


class Helper
{

    public static function knum($string){
        $string = str_replace(array('1','2','3','4','5','6','7','8','9','0'),
            array('១','២','៣','៤','៥','៦','៧','៨','៩','០'), $string);
        return $string;
    }
}
