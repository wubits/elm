<?php

namespace App\Providers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        Carbon::setLocale(config('app.locale'));
        Blueprint::macro('commonFields', function () {
            $this->timestamp('created_at')->useCurrent();
            $this->foreignId('created_by')->nullable()
                ->constrained('users')->onDelete('SET NULL');
            $this->timestamp('updated_at')->useCurrentOnUpdate();
            $this->foreignId('updated_by')->nullable()
                ->constrained('users')->onDelete('SET NULL');
        });
    }
}
