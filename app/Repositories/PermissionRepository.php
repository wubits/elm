<?php


namespace App\Repositories;


use App\Contracts\Repositories\PermissionRepositoryInterface;
use Spatie\Permission\Contracts\Permission;
use Spatie\Permission\Models\Role;

class PermissionRepository implements PermissionRepositoryInterface
{

    /**
     * @var Permission
     */
    private $permission;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    public function latest(){
        return $this->permission->get();
    }

    public function one($id){
        return \Spatie\Permission\Models\Permission::findById($id);
    }

    public function idArrayByRoleId($id){
        $role = Role::findById($id);
        return $role->permissions->pluck('id')->toArray();
    }

}
