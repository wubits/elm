<?php


namespace App\Repositories;


use App\Contracts\Repositories\PermissionRepositoryInterface;
use App\Contracts\Repositories\RoleRepositoryInterface;
use Spatie\Permission\Models\Role;

class RoleRepository implements RoleRepositoryInterface
{

    /**
     * @var Role
     */
    private $role;
    /**
     * @var PermissionRepositoryInterface
     */
    private $permissionRepository;

    public function __construct(Role $role, PermissionRepositoryInterface $permissionRepository)
    {
        $this->role = $role;
        $this->permissionRepository = $permissionRepository;
    }

    public function latest(){
        return $this->role->get();
    }

    public function one($roleId){
        return $this->role->findById($roleId);
    }

    public function create($data){
        $role = $this->role->create($data);
        $role->syncPermissions($data['permissions']);
        return $this->role;

    }

    public function update($roleId, $data){
        $role = $this->role->findById($roleId);
        $role->name = $data['name'];
        $role->save();
        $role->syncPermissions($data['permissions']);
        return $role;
    }

    public function delete($roleId){
        return Role::destroy($roleId);
    }

    public function allPermissions(){
        return $this->permissionRepository->latest();
    }

    public function permissionIDsArrayByRole($roleId){
        return $this->permissionRepository->idArrayByRoleId($roleId);
    }
}
