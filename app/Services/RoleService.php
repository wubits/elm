<?php


namespace App\Services;


use App\Contracts\Repositories\RoleRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class RoleService
{

    /**
     * @var RoleRepositoryInterface
     */
    private $roleRepository;

    public function __construct(RoleRepositoryInterface $roleRepository)
    {

        $this->roleRepository = $roleRepository;
    }

    public function create($data)
    {
        $validator = Validator::make($data,[
            'name' => 'required|unique:roles,name',
            'permissions' => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->messages();
        }else{
            return $this->roleRepository->create($data);
        }
    }

    public function update($id, $data){
        $validator = Validator::make($data,[
            'name' => 'required|unique:roles,name',
            'permissions' => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->messages();
        }else{
            return $this->roleRepository->update($id, $data);
        }
    }
}
