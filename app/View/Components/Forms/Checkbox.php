<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Checkbox extends Component
{
    public $value = '';
    public $field = '';
    public $selected = array();

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($value, $field, $selected)
    {
        $this->value = $value;
        $this->field = $field;
        $this->selected = $selected;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.forms.checkbox');
    }

    /**
     * Determine if the given option is the current selected option.
     *
     * @param  string  $option
     * @return bool
     */
    public function isChecked()
    {
        return in_array($this->value, $this->selected);
    }
}
