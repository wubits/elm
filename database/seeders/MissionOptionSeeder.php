<?php

namespace Database\Seeders;

use App\Models\Budget;
use App\Models\MissionRole;
use App\Models\Objective;
use App\Models\Transportation;
use Illuminate\Database\Seeder;

class MissionOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Objective::create([ 'name' => 'តាមដានអភិបាល']);
        Objective::create([ 'name' => 'វាយតម្លៃ']);
        Transportation::create([ 'name' => 'រថយន្តរដ្ឋ','plate_number' => 'កខគឃង៉ុក']);
        Transportation::create([ 'name' => 'រថយន្តផ្ទាល់ខ្លួន','plate_number' => 'លេខអីលេខទៅ']);
        Budget::create([ 'name' => 'ថវិការដ្ឋ']);
        Budget::create([ 'name' => 'ថវិកាគម្រង']);
        MissionRole::create(['name' => 'ប្រធាន']);
        MissionRole::create(['name' => 'សមាជិក']);
    }
}
