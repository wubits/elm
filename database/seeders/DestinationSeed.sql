insert  into `provinces`(`id`,`name`,`created_at`,`created_by`,`updated_at`,`updated_by`) values
(1,'បន្ទាយមានជ័យ','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(2,'បាត់ដំបង','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(3,'កំពង់ចាម','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(4,'កំពង់ឆ្នាំង','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(5,'កំពង់ស្ពឺ','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(6,'កំពង់ធំ','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(7,'កំពត','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(8,'កណ្ដាល','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(9,'កោះកុង','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(10,'ក្រចេះ','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(11,'មណ្ឌលគិរី','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(12,'ភ្នំពេញ','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(13,'ព្រះវិហារ','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(14,'ព្រៃវែង','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(15,'ពោធិ៍សាត់','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(16,'រតនគិរី','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(17,'សៀមរាប','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(18,'ព្រះសីហនុ','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(19,'ស្ទឹងត្រែង','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(20,'ស្វាយរៀង','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(21,'តាកែវ','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(22,'ឧត្តរមានជ័យ','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(23,'កែប','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(24,'ប៉ៃលិន','2020-12-17 11:08:32',NULL,'0000-00-00 00:00:00',NULL),
(25,'ត្បូងឃ្មុំ','2020-12-17 11:08:32',NULL,'2020-12-17 11:08:40',NULL);

insert  into `operational_districts`(`id`,`name`,`province_id`,`created_at`,`created_by`,`updated_at`,`updated_by`) values
(1,'មង្គលបូរី',1,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(2,'ប៉ោយប៉ែត',1,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(3,'ព្រះ​នេត្រ​ព្រះ',1,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(4,'ថ្មពួក',1,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(5,'ថ្មគោល',2,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(6,'មោងឫស្សី',2,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(7,'សំពៅលូន',2,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(8,'បាត់ដំបង',2,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(9,'សង្កែ',2,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(10,'ចំការលើ-ស្ទឹងត្រង់',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(11,'ជើងព្រៃ - បាធាយ',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(12,'កំពង់ចាម - កំពង់សៀម',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(13,'ក្រូចឆ្មារ-ស្ទឹងត្រង់',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(14,'មេមត់',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(15,'អូររាំងឱ-កោះសូទិន',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(16,'ពញាក្រែក - ដំបែ',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(17,'ព្រៃឈរ-កងមាស',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(18,'ស្រីសន្ធរ-កងមាស',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(19,'ត្បូងឃ្មុំ-ក្រូចឆ្មារ',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(20,'ស្ទឹងត្រង់',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(21,'បាធាយ',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(22,'កំពង់ឆ្នាំង',4,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(23,'កំពង់ត្រឡាច',4,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(24,'បរិបូណ៍',4,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(25,'កំពង់ស្ពឺ',5,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(26,'គងពិសី',5,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(27,'ឧដ្ដុង',5,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(28,'បារាយណ៍សន្ទុក',6,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(29,'កំពង់ធំ',6,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(30,'ស្ទោង',6,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(31,'អង្គរជ័យ',7,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(32,'ឈូក',7,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(33,'កំពង់ត្រាច',7,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(34,'កំពត',7,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(35,'អង្គស្នួល',8,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(36,'កៀនស្វាយ',8,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(37,'កោះធំ',8,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(38,'ខ្សាច់កណ្តាល',8,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(39,'មុខកំពូល',8,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(40,'ពញាឮ',8,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(41,'ស្អាង',8,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(42,'តាខ្មៅ',8,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(43,'ល្វាឯម',8,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(44,'ស្មាច់មានជ័យ',9,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(45,'ស្រែអំបិល',9,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(46,'ឆ្លូង',10,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(47,'ក្រចេះ',10,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(48,'សែនមនោរម្យ',11,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(49,'មេគង្គ',12,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(50,'ចតុមុខ',12,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(51,'ពោធិសែនជ័យ',12,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(52,'បាសាក់',12,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(53,'សែន សុខ',12,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(54,'ត្បែងមានជ័យ',13,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(55,'កំចាយមារ',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(56,'កំពង់ត្របែក',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(57,'មេសាង',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(58,'អ្នកលឿង',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(59,'ពារាំង',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(60,'ព្រះស្តេច',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(61,'ស្វាយអន្ទរ',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(62,'បាកាន',15,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(63,'សំពៅមាស',15,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(64,'បានលុង',16,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(65,'ក្រឡាញ់',17,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(66,'សៀមរាប',17,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(67,'សូទ្រនិគម',17,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(68,'អង្គរជុំ',17,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(69,'ព្រះសីហនុ',18,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(70,'ស្ទឹងត្រែង',19,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(71,'ជីភូ',20,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(72,'រមាសហែក',20,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(73,'ស្វាយរៀង',20,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(74,'អង្គរការ',21,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(75,'បាទី',21,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(76,'ដូនកែវ',21,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(77,'គិរីវង់',21,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(78,'ព្រៃកប្បាស',21,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(79,'សំរោង',22,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(80,'កែប',23,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(81,'ប៉ៃលិន',24,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(82,'ស៊ីធរកណ្តាល',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(83,'ជើងព្រៃ',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(84,'ចំការលើ',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(85,'ក្រូចឆ្មារ',25,'2020-12-17 11:09:55',NULL,'2020-12-17 11:10:12',NULL),
(86,'ត្បូងឃ្មុំ',25,'2020-12-17 11:09:55',NULL,'2020-12-17 11:10:12',NULL),
(87,'មេមត់',25,'2020-12-17 11:09:55',NULL,'2020-12-17 11:10:11',NULL),
(88,'អូរាំងឱ',25,'2020-12-17 11:09:55',NULL,'2020-12-17 11:10:11',NULL),
(89,'ពញាក្រែក - តំបែរ',25,'2020-12-17 11:09:55',NULL,'2020-12-17 11:10:10',NULL),
(90,'ភ្នំស្រួច',5,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(91,'ដង្កោ',12,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(92,'ក្រុងព្រៃវែង',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(93,'បរកែវ',16,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(94,'ស្វាយទាប',20,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(95,'បាភ្នំ',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(96,'លើកដែក',8,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(97,'កោះ​អណ្តែត',21,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(98,'ក្រវ៉ាញ',15,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(99,'ក្រគរ',15,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(100,'ព្រែក​ព្នៅ',12,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(101,'ពញាក្រែក',25,'2020-12-17 11:09:55',NULL,'2020-12-17 11:10:10',NULL),
(102,'តំបែរ',25,'2020-12-17 11:09:55',NULL,'2020-12-17 11:10:09',NULL),
(103,'សួង',25,'2020-12-17 11:09:55',NULL,'2020-12-17 11:10:09',NULL),
(104,'កោះសូទិន',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(105,'ព្រៃឈរ',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(106,'ស្រីសន្ធរ',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(107,'កងមាស',3,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(108,'អន្លង់វែង',22,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(109,'ពាមជរ៍',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(110,'កញ្ជ្រៀច',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(111,'ពាមរក៍',14,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(112,'កោះស្លា',7,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL),
(113,'សេរីសោភ័ណ',1,'2020-12-17 11:09:55',NULL,'0000-00-00 00:00:00',NULL);

insert  into `health_facility_types`(`id`,`name`,`created_at`,`created_by`,`updated_at`,`updated_by`) values
(1,'មន្ទីពេទ្យ','2020-12-17 11:13:49',NULL,'0000-00-00 00:00:00',NULL),
(2,'មណ្ឌលសុខភាព','2020-12-17 11:14:04',NULL,'0000-00-00 00:00:00',NULL),
(3,'អតីតពេទ្យស្រុក','2020-12-17 11:14:18',NULL,'0000-00-00 00:00:00',NULL),
(4,'មន្ទីពេទ្យបង្អែក','2020-12-17 11:14:30',NULL,'0000-00-00 00:00:00',NULL),
(5,'មន្ទីពេទ្យជាតិ','2020-12-17 11:14:44',NULL,'0000-00-00 00:00:00',NULL),
(6,'មន្ទីពេទ្យខេត្ត','2020-12-17 11:14:49',NULL,'0000-00-00 00:00:00',NULL);
