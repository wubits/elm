<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Eloquent\Model::unguard();

        $this->call(PermissionSeeder::class);
        $this->command->info('User, permissions, and roles are seeded!');

        $path = database_path('seeders/EmployeeSeed.sql');
        DB::unprepared(file_get_contents($path));
        $this->command->info('Employees, positions, contract types, and departments table seeded!');

        DB::unprepared(file_get_contents(database_path('seeders/DestinationSeed.sql')));
        $this->command->info('Provinces, operational districts, and health facility types table are seeded!');

        DB::unprepared(file_get_contents(database_path('seeders/HealthFacilitySeed.sql')));
        $this->command->info('Health facilities table is seeded!');

        $this->call(MissionOptionSeeder::class);
        $this->command->info('Mission options are seeded!');
    }
}
