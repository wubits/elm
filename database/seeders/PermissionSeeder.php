<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'mission-list', 'group' => 'mission']);
        Permission::create(['name' => 'mission-edit', 'group' => 'mission']);
        Permission::create(['name' => 'mission-delete', 'group' => 'mission']);
        Permission::create(['name' => 'mission-approve', 'group' => 'mission']);
        Permission::create(['name' => 'mission-cancel', 'group' => 'mission']);
        Permission::create(['name' => 'mission-view', 'group' => 'mission']);
        Permission::create(['name' => 'mission-create', 'group' => 'mission']);

        Permission::create(['name' => 'user-list', 'group' => 'user']);
        Permission::create(['name' => 'user-create', 'group' => 'user']);
        Permission::create(['name' => 'user-edit', 'group' => 'user']);
        Permission::create(['name' => 'user-delete', 'group' => 'user']);

        Permission::create(['name' => 'role-list', 'group' => 'role']);
        Permission::create(['name' => 'role-create', 'group' => 'role']);
        Permission::create(['name' => 'role-edit', 'group' => 'role']);
        Permission::create(['name' => 'role-delete', 'group' => 'role']);

        Permission::create(['name' => 'employee-list', 'group' => 'employee']);
        Permission::create(['name' => 'employee-create', 'group' => 'employee']);
        Permission::create(['name' => 'employee-edit', 'group' => 'employee']);
        Permission::create(['name' => 'employee-delete', 'group' => 'employee']);

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'admin-staff']);
        $role1->givePermissionTo('mission-edit');
        $role1->givePermissionTo('mission-list');
        $role1->givePermissionTo('mission-view');

        $role1->givePermissionTo('employee-edit');
        $role1->givePermissionTo('employee-create');
        $role1->givePermissionTo('employee-list');

        $role2 = Role::create(['name' => 'admin-manager']);
        $role2->givePermissionTo('mission-approve');
        $role2->givePermissionTo('mission-cancel');
        $role2->givePermissionTo('mission-delete');
        $role2->givePermissionTo('mission-edit');
        $role2->givePermissionTo('mission-create');
        $role2->givePermissionTo('mission-list');
        $role2->givePermissionTo('mission-view');

        $role2->givePermissionTo('employee-edit');
        $role2->givePermissionTo('employee-create');
        $role2->givePermissionTo('employee-list');
        $role2->givePermissionTo('employee-delete');


        $role3 = Role::create(['name' => 'superadmin']);
        // gets all permissions via Gate::before rule; see AuthServiceProvider

        // create demo users
        $user = \App\Models\User::factory()->create([
            'name' => 'Superadmin',
            'email' => 'super@admin.com',
            'password' => bcrypt('123'),
        ]);
        $user->assignRole($role3);

        $user = \App\Models\User::factory()->create([
            'name' => 'Example Staff',
            'email' => 'staff@admin.com',
            'password' => bcrypt('123'),
        ]);
        $user->assignRole($role1);

        $user = \App\Models\User::factory()->create([
            'name' => 'Example Manager',
            'email' => 'manager@admin.com',
            'password' => bcrypt('123'),
        ]);
        $user->assignRole($role2);
    }
}
