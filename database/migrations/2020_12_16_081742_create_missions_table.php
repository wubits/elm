<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missions', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('reference_document')->nullable();
            $table->string('notes')->nullable();
            $table->date('leave_date');
            $table->date('return_date')->nullable();
            $table->unsignedBigInteger('transportation_id');
            $table->unsignedBigInteger('objective_id');
            $table->unsignedBigInteger('budget_id');
            $table->unsignedBigInteger('status_id')->default(1);
            $table->commonFields();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missions');
    }
}
