<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missionables', function (Blueprint $table) {
            $table->unsignedBigInteger('mission_id');
            $table->unsignedBigInteger('missionable_id');
            $table->string('missionable_type');
            $table->primary(array('mission_id','missionable_id', 'missionable_type'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missionables');
    }
}
