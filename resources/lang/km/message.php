<?php

return [
    'error_mission_pick_one_destination' => 'សូមជ្រើសរើសទិសដៅយ៉ាងហោចណាស់អោយបានមួយ',
    'error_mission_pick_one_member' => 'សូមជ្រើសរើសសមាជិកយ៉ាងហោចណាស់អោយបានមួយ',
    'success_mission_saved' => 'បេសកកម្មត្រូវបានរក្សាទុកដោយជោគជ័យ',
    'success_mission_updated' => 'បេសកកម្មត្រូវបានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
    'success_mission_deleted' => 'បេសកកម្មត្រូវបានលុបដោយជោគជ័យ',
    'no_permission' => 'លោកអ្នកមិនមានសិទ្ធធ្វើការកែប្រែ ឬលុបទិន្នន័យនោះទេ!!!',
    'redirect_back'=>'ទំព័រនេះផុតសុពលភាពក្នុងរយៈពេល ៥ វិនាទី',
    'error_mission_add_destination' => 'ទិសដៅបេសកកម្មបានបន្ថែមតែមិនបានជ្រើសរើសទិន្នន័យ',
    'no_permission' => 'អ្នកមិនមានសិទ្ធក្នុងទំព័រនេះទេ',
    'redirect_back'=>'ទំព័រនេះផុតសុពលភាពក្នុងរយៈពេល ៥ វិនាទី',
    'success_saved' => 'ត្រូវបានរក្សាទុកដោយជោគជ័យ',
    'success_updated' => 'ត្រូវបានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
    'success_deleted' => 'ត្រូវបានលុបដោយជោគជ័យ',


    'error_mission_leave_date_is_required'=>'សូមបញ្ចូលថ្ងៃចេញដំណើរ',
    'error_mission_return_date_is_required'=>'សូមបញ្ចូលងៃចត្រលប់មកវិញ',
    'error_mission_objective_is_required'=>'សូមបញ្ចូលគោលបំណង',
    'error_mission_transportation_is_required'=>'សូមបញ្ចូលមធ្យោបាយធ្វើដំណើរ',
    'message.error_mission_budget_is_required'=>'សូមបញ្ចូលថវិកាផ្គត់ផ្គង់',
    'error_reference_document_is_required'=>'សូមបញ្ចូលឯកសារយោង',
];
