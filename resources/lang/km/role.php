<?php

return [
    'users' => 'អ្នកប្រើប្រាស់',
    'display roles' => 'បង្ហាញតួនាទី',
    'permissions' => 'អំណាច',
    'create​ role permission' => 'បង្កើតតួនាទីអ្នកប្រើប្រាស់',
    'create role'  => 'បង្កើតតួនាទី',
    'name' => 'ឈ្មោះនៃទួនាទី',
    'define_permission'=> 'កំណត់ការអនុញ្ញាតក្នុងការប្រើប្រាស់',
    'edit role permission' => 'កែប្រែតួនាទីអ្នកប្រើប្រាស់',
    'role' => 'តួនាទី',
    // VALIDATION
    'name_required' => 'អ្នកត្រូវតែបញ្ចូលឈ្មោះរបស់តួនាទី',
    'name_unique' => 'ឈ្មោះរបស់តួនាទីនេះមានរួចទៅហើយ',
    'permissions_required' => 'អ្នកត្រូវតែជ្រើរើសការអនុញ្ញាតសម្រាប់តួនាទីនេះ',
];
