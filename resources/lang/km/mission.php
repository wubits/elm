<?php

return [
    'index' => 'តារាងបេសកកម្ម',
    'code' => 'លេខកូដ',
    'destination' => 'ទិសដៅ',
    'member' => 'សមាជិក',
    'leave_date' => 'ថ្ងៃចេញដំណើរ',
    'return_date' => 'ថ្ងៃត្រលប់មកវិញ',
    'objective' => 'គោលបំណង',
    'create' => 'បញ្ចូលបេសកកម្មថ្មី',
    'reference_document' => 'ឯកសារយោង',
    'transportation' => 'មធ្យោបាយធ្វើដំណើរ',
    'budget' => 'ថវិកាផ្គត់ផ្គង់',
    'note' => 'កំណត់ចំណាំផ្សេងៗ',
    'province' => 'ខេត្ត',
    'operational_district' => 'ស្រុកប្រត្តិបត្តិ',
    'health_facility' => 'មូលដ្ឋានសុខាភិបាល',
    'employee' => 'បុគ្គលិក',
    'role' => 'តួនាទី',
    'edit' => 'កែប្រែបេសកកម្ម',
];
