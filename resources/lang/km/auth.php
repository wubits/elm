<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'អ៊ីម៉ែល ឬក៏លេខកូដរបស់អ្នកមិនត្រឹមត្រូវនោះទេ',
    'password' => 'លេខកូដរបស់អ្នកមិនត្រឹមត្រូវនោះទេ',
    'throttle' => 'អ្នកត្រូវបានប្លក់ដោយកំពុងព្យាយាមចូលប្រព័ន្ធតាមអ៊ីម៉ែល ឬក៏លេខកូដរបស់អ្នកមិនត្រឹមត្រូវនោះទេ',

];
