@props([
'class' => '',
])

<th {{ $attributes->merge(['class' => 'border-top-0 '. $class ]) }}>{{ $slot }}</th>
