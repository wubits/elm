@props([
    'id' => '',
    'show' => '',
    'edit' => '',
    'destroy' => '',
    'viewPermission' => '',
    'editPermission' => '',
    'deletePermission' => '',
])
@can($viewPermission)
    @if(!empty($show))
        <x-forms.button href="{{ $show }}" class="btn-sm float-left"><i class="fas fa-info-circle"></i></x-forms.button>
    @endif
@endcan

@can($editPermission)
    @if(!empty($edit))
        <x-forms.button href="{{ $edit }}" class="btn-success btn-sm float-left"><i class="fas fa-pencil-alt"></i></x-forms.button>
    @endif
@endcan

@can($deletePermission)
    @if(!empty($destroy))
        <form action="{{ $destroy }}" id="delete_form_{{ $id }}" method="POST" class="form-inline float-left">
            @method('DELETE')
            @csrf
            <x-forms.button id="btn_delete_{{ $id }}" class="btn-danger btn-sm"><i class="fas fa-trash-alt"></i></x-forms.button>
        </form>
    @endif
@endcan

@push('scripts')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        $("#btn_delete_{{ $id }}").on('click',function(e) { //also can use on submit
            swal({
                title: "{{ __('Are you sure?') }}",
                text: "{{ __('Once deleted, you cannot recover') }}",
                icon: "warning",
                buttons: ["{{ __('Cancel') }}", "{{ __('Ok') }}"],
                dangerMode: true,
            })
            .then(function(value) {
                if (value) {
                    swal("{{ __('Successfully deleted!') }}", {
                        icon: "success",
                        buttons: false,
                    });
                    $('#delete_form_{{ $id }}').submit();
                }
            });
        });
    </script>
@endpush
