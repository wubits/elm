<table {{ $attributes->merge(['class' => 'table-striped table-bordered table table-hover ']) }}>
    <thead>
        <tr class="text-white" style="background-color: #3e85d3">
           {{ $header }}
        </tr>
    </thead>
    <tbody>
            {{ $slot }}
    </tbody>
</table>
