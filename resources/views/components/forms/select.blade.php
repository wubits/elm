@props([
'label' => '',
'name' => '',
'value' => '',
'initial' => '',
'collection' => '',
'start' => '',
'class' => '',
'selected' => '',
'selecteds' => [],
'modal' => '',
])

<x-forms.select.base name="{{ $name }}" label="{{ $label }}" value="{{ $value }}" initial="{{ $initial }}" start="{{ $start }}" class="{{ $class }}" modal="{{ $modal }}" {{ $attributes }}>
    @if(!empty($collection))
        @foreach ($collection as $item)
            <option value="{{ $item->id }}" @if($item->id == $selected) selected @endif @if(in_array($item->id, $selecteds)) selected @endif>{{ $item->name }}</option>
        @endforeach
    @endif

</x-forms.select.base>
