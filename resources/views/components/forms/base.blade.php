@props([
'route' => '',
'method' => 'post',
'backTo' => url()->previous(),
])
<form action="{{ $route }}" method="POST" {{ $attributes }}>
    @method($method)
    @csrf
    <x-layouts.card>
        <x-slot name="title">
            <div class="float-left">
                {{ $title }}
            </div>
            <div class="float-right">
                <x-forms.back backTo='{{ $backTo }}'></x-forms.back>
                <x-forms.submit><i class="fas fa-save mr-1"></i>{{__('app.submit')}}</x-forms.submit>
            </div>
        </x-slot>
        <div class="col-12">
            <div class="row p-3">
                <div class="col-12">
                    {{ $slot }}
                </div>
            </div>
        </div>
    </x-layouts.card>
</form>
