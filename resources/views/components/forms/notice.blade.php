@props([
    'type' => '',
    'class' => '',
])

    <div {{ $attributes->merge(['class' => 'notice ' . $class]) }}>
        <strong class="mr-1">{{ $type }}</strong>{{ $slot }}
    </div>
