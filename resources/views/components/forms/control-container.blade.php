@props([
    'name' => '',
    'class' => '',
])

<div {{ $attributes }} class="@error($name) border border-danger rounded @enderror {{ $class }}">
    {{ $slot }}
    @error($name)
    <small class="my-1 text-danger">{{ $message }}</small>
    @enderror
</div>
