@props([
'label' => '',
'name' => '',
'value' => '',
])
<div class="form-group">
    <label class="font-weight-bold @error($name) text-danger @endisset">{{ $label }}</label>
    <textarea name="{{ $name }}" class="form-control">{{ $value }}</textarea>
    @error($name)
        <small class="my-1 text-danger">{{ $message }}</small>
    @enderror
</div>
