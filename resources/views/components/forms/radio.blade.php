@props([
'options' => [],
'ids' => [],
'name' => 'name',
'selected' => '',
'label' => '',
])

@push('styles')
    <link href="{{ asset('css/radio.css') }}" rel="stylesheet">
@endpush
<div class="row mb-2">
    <label class="col-12 font-weight-bold @error($name) text-danger @endisset">{{ $label }}</label>
    <div class="col-12">
        @foreach($options as $key => $option)
            <div class="float-left mr-2">
                <input type="radio" name="{{ $name }}" value="{{ $key }}" @if($selected == $option) checked @endif>
                <label>{{ $option }}</label>
            </div>
        @endforeach
    </div>
</div>
