@props([
    'value' => '',
    'label' => '',
])

<option value="{{ $value }}">{{ $label }}</option>
