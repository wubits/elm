@props([
'label' => '',
'name' => '',
'value' => '',
'initial' => '',
'start' => '',
'class' => '',
'modal' => '',
])

<div class="form-group">
    @if(!isset($attributes['no-label']))
        <label class="font-weight-bold @error($name) text-danger @endisset">{{ $label }}</label>
    @endif
        <div class="input-group mb-3">
            <select name="{{ $name  }}" class="w-75 form-control @error($name)is-invalid @enderror {{ $class }}"  {{ $attributes }}>
                <option value="{{ $start }}" @if(!$attributes['noSelected']) selected @endif>{{ __('app.choose') }}{{ $label }}</option>
                {{ $slot }}
            </select>
            @if($modal != '')
                <button type="button" class="form-control btn btn-primary" data-toggle="modal" data-target="#{{ $modal }}Modal"><i class="fas fa-plus"></i></button>
            @endif
        </div>
        @error($name)
    <small class="my-1 text-danger">{{ $message }}</small>
    @enderror
</div>
