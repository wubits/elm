@props([
    'name' => '',
])

<fieldset class="border py-1 px-4 @error($name) border-danger @enderror">
    <legend class="w-auto h6 font-weight-bold px-2">{{ $title }}</legend>
    <div class="row">
        {{ $slot }}
    </div>
</fieldset>
@error($name)
<small class="my-1 text-danger">{{ $message }}</small>
@enderror
