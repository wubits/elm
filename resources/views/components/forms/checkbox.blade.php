<div class="form-check my-3">
    <input class="form-check-input mr-2" type="checkbox" value="{{ $value }}" name="{{ $field }}" {{ $isChecked() ? 'checked' : '' }}>
    <label class="form-check-label" for="flexCheckDefault">{{ $slot }}</label>
</div>
