@props([
'label' => '',
'name' => '',
'value' => '',
])
<div class="form-group">
    <label class="font-weight-bold @error($name) text-danger   @endisset">{{ $label }}</label>
    <input type="date" class="form-control @error($name) is-invalid @endisset"
           name="{{ $name }}"
           value="{{ $value }}"/>
    @error($name)
    <small class="my-1 text-danger">{{ $message }}</small>
    @enderror
</div>
