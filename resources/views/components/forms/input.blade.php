@props([
'label' => '',
'placeholder' => '',
'name' => '',
'value' => '',
'type' => 'text',
])

<div class="form-group">
    <label class="font-weight-bold @error($name) text-danger @enderror">{{ $label }}</label>
    <input type="{{$type}}" class="form-control @error($name) is-invalid @enderror"
           placeholder="{{ $placeholder }}"
           name="{{ $name }}"
           value="{{ $value }}" {{ $attributes }}/>
    @error($name)
        <small class="my-1 text-danger">{{ $message }}</small>
    @enderror
</div>
