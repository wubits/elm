<button type="submit" {{ $attributes->merge(['class' => 'mr-1 btn btn-primary']) }}>{{ $slot }}</button>
