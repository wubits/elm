@props([
    'backTo' => url()->previous()
])

<x-forms.button class="btn-secondary" href="{{ $backTo }}"><i class="fas fa-chevron-circle-left mr-1"></i>{{__('app.back')}}</x-forms.button>
