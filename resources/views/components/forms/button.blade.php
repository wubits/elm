@props([
'show' => '',
'edit' => '',
'destroy' => '',
])

@if(!empty($show))
    <a href="{{ $show }}" {{ $attributes->merge(['class' => 'mr-1 btn btn-primary']) }}>{{ $slot }}</a>
@elseif(!empty($edit))
    <a href="{{ $edit }}" {{ $attributes->merge(['class' => 'mr-1 btn btn-primary']) }}>{{ $slot }}</a>
@elseif(!empty($destroy))
    <form action="{{ $destroy }}" method="POST" class="form-inline float-left">
        @method('DELETE')
        @csrf
        <x-forms.submit class="btn-danger btn-sm"><i class="fas fa-trash-alt"></i></x-forms.submit>
    </form>
@else
    <a {{ $attributes->merge(['class' => 'mr-1 btn btn-primary']) }}>{{ $slot }}</a>
@endif
