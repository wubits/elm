@props([
'index' => 0,
'start' => 0,
'provinces' => [],
'ods' => [],
'hfs' => [],
'selectedProvince' => '',
'selectedOd' => '',
'selectedHf' => '',
])
<tr id="row_{{ $index + $start }}">
    <td class="destination-no" scope="row">{{ $index + $start }}</td>
    <td><x-forms.select name="provinces[]" selected="{{ $selectedProvince }}" :collection="$provinces" class="data-provinces" no-label id="province-{{ $index + $start }}" label="{{ __('mission.province') }}"></x-forms.select></td>
    <td><x-forms.select name="operational_districts[]"  selected="{{ $selectedOd }}" :collection="$ods" class="data-od" no-label id="od-{{ $index + $start }}" label="{{ __('mission.operational_district') }}"></x-forms.select></td>
    <td>
        <x-forms.select.base name="health_facilities[]" label="{{ __('mission.health_facility') }}" no-label class="data-hf" id="fac-{{ $index + $start }}">
            @if(!empty($hfs))
                @foreach ($hfs as $item)
                    <option value="{{ $item->id }}" @if($item->id == $selectedHf) selected @endif>{{ $item->fullname }}</option>
                @endforeach
            @endif
        </x-forms.select.base>
    </td>
    <td><x-forms.button class="btn-sm remove btn-danger" data-id="'+trL+'"><i class="fas fa-minus-circle"></i></x-forms.button></td>
</tr>
