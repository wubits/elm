@props([
    'employees' => [],
    'roles' => [],
    'selectedEmployee' => 0,
    'selectedRole' => 0,
])


<tr id="rowMember_1">
    <td id="tablenoMember" scope="row">1</td>
    <td>
        <x-forms.select name="members[]" class="select-member" :collection="$employees" selected="{{ $selectedEmployee }}" no-label start="0" label="{{ __('mission.employee') }}"></x-forms.select>
    </td>
    <td>
        <x-forms.select name="roles[]" class="form-control" :collection="$roles" selected="{{ $selectedRole }}" no-label label="{{ __('mission.role') }}"></x-forms.select>
    </td>
    <td>
        <x-forms.button class="btn-sm removeMissionMember btn-danger" data-id="'+trL+'"><i class="fas fa-minus-circle"></i></x-forms.button>
    </td>
</tr>
