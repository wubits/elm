@props([
'submit' => '',
'title' => '',
])

<div wire:ignore.self class="modal fade" id="transportationModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $title }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    {{ $slot }}
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">{{ __('Cancel') }}</button>
                <button type="button" wire:click.prevent="save()" data-dismiss="modal" class="btn btn-primary close-modal">{{ __('Save') }}</button>
            </div>
        </div>
    </div>
</div>
