<nav class="navbar navbar-expand-md navbar-dark bg-blue shadow-sm">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ __('Admin System') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto ml-5">
                @auth
                <li class="nav-item">
                    {{-- <a class="nav-link" href="">{{ __('app.mission') }}</a> --}}
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link" href="{{ route('mission.index') }}" role="button">
                        {{ __('app.mission') }}
                    </a>

                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link" href="{{ route('employees.index') }}" role="button">
                        {{ __('employee.employee') }}
                    </a>
                </li>
                @endauth
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('app.login') }}</a>
                        </li>
                    @endif
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('app.register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('users.profile') }}"><i class="fas fa-user-alt"></i> &nbsp;&nbsp;{{ __('app.user_profile')}}</a>
                                <hr/>
                                @can('user-list')
                                    <a class="dropdown-item" href="{{ route('users.index') }}"><i class="fas fa-users-cog"></i>&nbsp;&nbsp; {{ __('role.users')}}</a>
                                @endcan
                                @can('role-list')
                                    <a class="dropdown-item" href="{{route('roles.index')}}"><i class="fas fa-user-tag"></i>&nbsp;&nbsp; {{ __('role.role')}}</a>
                                @endcan
                                <hr/>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt"></i>&nbsp;&nbsp; {{ __('app.logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
