@props([
'class' => ''
])
@if ($message = Session::get('success'))
    <div class="row mb-2">
        <x-forms.notice type="{{ __('app.success') }}" class="col-12 success">
           {{ $message }}
        </x-forms.notice>
    </div>
@endif

<div class="row bg-light bg-white shadow-sm rounded">
    <div class="col-12">

        <div {{ $attributes->merge(['class' => 'row p-3 bg-blue rounded-top ' . $class]) }}>
            <div class="col-lg-12 text-white">
                {{ $title }}
            </div>
        </div>

        <div class="row">
            {{ $slot }}
        </div>
    </div>
</div>
