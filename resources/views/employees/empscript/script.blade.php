<script>
$(document).on("change", "#department" , function() {
        var depId = $(this).val();
        $.ajax({
            type: 'GET',
            url: "/department/" + depId +"/positions",
            dataType: 'JSON',
            success: function (data) {
                $('select[name="position_id"').empty();
                var pos_html = '<option value="0">{{ __('app.choose') }}{{ __('employee.position') }}</option>';
                $('select[name="position_id"').append(pos_html);
                $.each(data, function(key, value) {
                    $('select[name="position_id"').append('<option value="'+ key +'">'+ value +'</option>');
                });
            },
        });

    });
</script>