<x-layout>
    <x-forms.base route="{{route('employees.store')}}" backTo=" {{ route('employees.index') }}">
        <x-slot name="title">
                <h4>{{ __('employee.create') }}</h4>
        </x-slot>
        <div class="employee">
            <div class="row">
                <div class="col-12">
                    <x-forms.input
                        label="{{ __('employee.name') }}"
                        name="fullname"
                        placeholder="{{__('employee.name') }}">
                    </x-forms.input>
                </div>
                <div class="col-12">
                    <x-forms.radio name="sex"
                                    label="{{ __('employee.sex') }}"
                                    :ids="array('ប្រុស'=>'male_id', 'ស្រី'=>'female_id')"
                                    :options="array('ប្រុស'=>'ប្រុស','ស្រី'=>'ស្រី')"></x-forms.radio>
                </div>
                <div class="col-12">
                    <x-forms.select
                        label="{{ __('employee.department') }}"
                        name="department"
                        id="department"
                        start="0"
                        :collection="$departments"
                        placeholder="{{ __('employee.department') }}">
                    </x-forms.select>
                </div>
                <div class="col-12">
                    <x-forms.select
                        label="{{ __('employee.position') }}"
                        name="position_id"
                        id="data-positions"
                        placeholder="{{ __('employee.position') }}">
                    </x-forms.select>
                </div>

                <div class="col-12">
                    <x-forms.textarea
                        label="{{ __('employee.note') }}"
                        name="notes">
                    </x-forms.textarea>
                </div>
            </div>
        </div>
        {{-- @dump($errors->all()) --}}
    </x-forms.base>

    <x-slot name="header">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    </x-slot>
    <x-slot name="endbody">
        @include('employees.empscript.script')
    </x-slot>
</x-layout>
