<x-layout>
    <x-layouts.card>
        <x-slot name="title">
            <div class="float-left">
                <h4 style="padding-top: 6px;">{{__('employee.employee')}}</h4>
            </div>
            <div class="float-right">
                @can('employee-create')
                   <x-forms.button color="success" href="{{route('employees.create')}}" style="font-size: 16px">{{__('employee.create')}}</x-forms.button>
                @endcan
            </div>
        </x-slot>
        <div class="col-12" style="margin-top:20px">
            <x-tables.base id="employee">
                <x-slot name="header">
                    <x-tables.header>{{__('employee.no')}}</x-tables.header>
                    <x-tables.header>{{__('employee.name')}}</x-tables.header>
                    <x-tables.header>{{__('employee.sex')}}</x-tables.header>
                    <x-tables.header>{{__('employee.position')}}</x-tables.header>
                    <x-tables.header>{{__('employee.department')}}</x-tables.header>
                    <x-tables.header>{{__('employee.action')}}</x-tables.header>
                </x-slot>
                @foreach ($employees as $index => $employee)
                    <tr>
                        <td>{{ $index+1 }}</td>
                        <td>{{$employee->fullname}}</td>
                        <td>{{$employee->sex}}</td>
                        <td>{{$employee->position->name}}</td>
                        <td>{{$employee->position->department->name}}</td>
                        <td class="edit">
                            <x-tables.action
                                id="{{ $index }}"
                                editPermission="employee-edit"
                                deletePermission="employee-delete"
                                edit="{{route('employees.edit', $employee->id) }}"
                                destroy="{{route('employees.destroy', $employee->id) }}">
                            </x-tables.action>
                        </td>
                    </tr>
                @endforeach
            </x-tables.base>
        </div>
    </x-layouts.card>
    <x-slot name="endbody">
        <script>
            $(document).ready( function (){
                $('#employee').DataTable(
                    {
                        "language": {
                            url : "https://cdn.datatables.net/plug-ins/1.10.22/i18n/Khmer.json"

                        },
                        "iDisplayLength": 25
                    }
                );
            });
        </script>
    </x-slot>

     <x-slot name="header">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" defer></script>
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" />
    </x-slot>
</x-layout>
