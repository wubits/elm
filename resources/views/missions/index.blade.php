<x-layout style="max-width: 100%">
    <x-layouts.card>
        <x-slot name="title">
            <div class="float-left">
                <h4>{{__('mission.index')}}</h4>
            </div>
            @can('mission-create')
                <div class="float-right">
                    <a href="{{ route('mission.create') }}" class="btn-primary btn ">បញ្ជូលបេសកម្មថ្មី</a>
                </div>
            @endcan
        </x-slot>

        <div class="col-12 py-2">
            <x-tables.base id="table_id">
                <x-slot name="header">
                    <x-tables.header>{{__('app.no')}}</x-tables.header>
                    <x-tables.header>{{__('mission.code')}}</x-tables.header>
                    <x-tables.header>{{__('mission.destination')}}</x-tables.header>
                    <x-tables.header>{{__('mission.member')}}</x-tables.header>
                    <x-tables.header>{{__('mission.leave_date')}}</x-tables.header>
                    <x-tables.header>{{__('mission.return_date')}}</x-tables.header>
{{--                    <x-tables.header>{{__('mission.objective')}}</x-tables.header>--}}
                    <x-tables.header>{{__('app.action')}}</x-tables.header>
                </x-slot>

                @php
                    $i=0;
                @endphp
                @foreach ($missions as $index => $mission)
                    <tr>
                        <td>{{ $index+1 }}</td>
                        <td>{{ Helper::knum($mission->code) }}</td>
                        <td style="width: 20%;font-size: 12px;">
                            @foreach ($mission->destinations as $destination)
                                <i class="fa fa-h-square" ></i>  {{ $destination }}<br/>
                            @endforeach
                        </td>
                        <td style="width: 25%;">
                            @foreach ($mission->members as  $member)
                            <b>{{ $member->fullname }}</b> {{$member->position->name}}<br/>
                            @endforeach
                        </td>
                        <td>{{ $mission->khLeaveDate }}</td>
                        <td>{{ $mission->khReturnDate }}</td>
{{--                        <td>{{ $mission->objective->name }}</td>--}}
                        <td>
                            <x-tables.action
                                id="{{ $index }}"
                                viewPermission="mission-detail"
                                editPermission="mission-edit"
                                deletePermission="mission-delete"
                                show="{{ route('mission.show', $mission->id) }}"
                                edit="{{ route('mission.edit', $mission->id) }}"
                                destroy="{{ route('mission.destroy', $mission->id) }}"></x-tables.action>
                        </td>
                    </tr>
                @endforeach

            </x-tables.base>
        </div>

    </x-layouts.card>

    <x-slot name="endbody">
        <script>
            $(document).ready( function () {
                $('#table_id').DataTable(
                    {
                        "language": {
                            url : "https://cdn.datatables.net/plug-ins/1.10.22/i18n/Khmer.json"
                        },
                    }
                );
            } );
        </script>

        <script>
            $('#reback').on("click",function() {
                var missionID = $(this).attr('value');
                $.ajax({
                    type: 'GET',
                    url: "{{ url('mission/reback') }}/"+missionID,
                    //data: { missionID: missionID },
                    dataType: 'JSON',
                    success: function (data) {
                        //  alert('lengDara');
                    },
                });
            });
        </script>
    </x-slot>

    <x-slot name="header">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" defer></script>
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" />
    </x-slot>
</x-layout>

