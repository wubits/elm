<x-layout>
    <x-forms.base route="{{ route('mission.update', $mission->id) }}" method="PUT" enctype="multipart/form-data" backTo=" {{ route('mission.index') }}">
        <x-slot name="title">
            <h4>{{ __('mission.edit') }} {{ $mission->code }}</h4>
        </x-slot>
        <div class="form-row mb-4">
            <div class="col-sm-3" >
                <x-forms.input value="{{ $mission->code }}" name="code" label="{{ __('mission.code') }}"></x-forms.input>
            </div>
        </div>

        <x-forms.section>{{ __('app.general_information') }}</x-forms.section>
        <div class="form-row mb-4">
            <div class="col-sm-6">
                <x-forms.input value="{{ $mission->reference_document }}" name="reference_document" label="{{ __('mission.reference_document') }}"></x-forms.input>
            </div>
            <div class="col-sm-3">
                <x-forms.date value="{{ $mission->leave_date->format('Y-m-d') }}" name="leave_date" label="{{ __('mission.leave_date') }}"></x-forms.date>
            </div>
            <div class="col-sm-3">
                <x-forms.date value="{{ $mission->return_date->format('Y-m-d') }}" name="return_date" label="{{ __('mission.return_date') }}"></x-forms.date>
            </div>
        </div>
        <div class="form-row mb-4">
            <div class="col">
                <x-forms.select name="objective_id" label="{{ __('mission.objective') }}" :collection="$objectives" selected="{{ $mission->objective_id }}"></x-forms.select>
            </div>
            <div class="col">
                <livewire:transportation.select-box :transportation="$mission->transportation->id"></livewire:transportation.select-box>
            </div>

            <div class="col">
                <x-forms.select name="budget_id" label="{{ __('mission.budget') }}" :collection="$budgets" selected="{{ $mission->budget_id }}"></x-forms.select>
            </div>
        </div>
        <div class="form-row mb-4">
            <div class="col">
                <x-forms.textarea name="notes" label="{{ __('mission.note') }}" value="{{ $mission->notes }}"></x-forms.textarea>
            </div>
        </div>

        <x-forms.section>{{__('mission.destination')}}</x-forms.section>
        <div class="form-row mb-4">
            <x-forms.control-container class="col" name="provinces.*">
                <table class="table" id="tb-distination">
                    <thead>
                    <tr>
                        <x-tables.header>{{ __('#') }}</x-tables.header>
                        <x-tables.header class="w-25">{{ __('mission.province') }}</x-tables.header>
                        <x-tables.header class="w-25">{{ __('mission.operational_district') }}</x-tables.header>
                        <x-tables.header class="w-25">{{ __('mission.health_facility') }}</x-tables.header>
                        <x-tables.header><x-forms.button class="btn-sm add-destination-row"><i class="fas fa-plus-circle"></i></x-forms.button></x-tables.header>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mission->provinces as $index => $province)
                        <x-missions.edit-destination
                            index="{{ $index+1 }}"
                            selectedProvince="{{ $province->id }}"
                            :provinces="$provinces"
                            :ods="$province->operationalDistricts"></x-missions.edit-destination>
                    @endforeach
                    @foreach($mission->operationalDistricts as $index => $od)
                        <x-missions.edit-destination
                            index="{{ $index+1 }}"
                            :provinces="$provinces"
                            selectedProvince="{{ $od->province->id }}"
                            :ods="$od->province->operationalDistricts"
                            selectedOd="{{ $od->id }}"
                            :hfs="$od->healthFacilities"
                            start="{{ $province_count }}"></x-missions.edit-destination>
                    @endforeach
                    @foreach($mission->healthFacilities as $index => $hf)
                        <x-missions.edit-destination
                            index="{{ $index+1 }}"
                            :provinces="$provinces"
                            selectedProvince="{{ $hf->operationalDistrict->province->id }}"
                            :ods="$hf->operationalDistrict->province->operationalDistricts"
                            selectedOd="{{ $hf->operationalDistrict->id }}"
                            :hfs="$hf->operationalDistrict->healthFacilities"
                            selectedHf="{{ $hf->id }}"
                            start="{{ $od_count }}"></x-missions.edit-destination>
                    @endforeach
                    </tbody>
                </table>
            </x-forms.control-container>
        </div><hr>

        <x-forms.section>{{ __('mission.member') }}</x-forms.section>


        <div class="form-row mb-4">
            <x-forms.control-container class="col" name="members.*">
                <table class="table" id="missionMember">
                    <thead>
                    <tr>
                        <x-tables.header>{{ __('#') }}</x-tables.header>
                        <x-tables.header class="w-75">{{ __('employee.name') }}</x-tables.header>
                        <x-tables.header hidden class="w-25">{{ __('employee.role') }}</x-tables.header>
                        <x-tables.header><x-forms.button class="btn-sm addMissionMember"><i class="fas fa-plus-circle"></i></x-forms.button></x-tables.header>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($mission->members as $index => $member)
                        <tr id="rowMember_{{ $index+1 }}">
                            <td class="member-no" scope="row">{{ $index+1 }}</td>
                            <td>
                                <x-forms.select name="members[]" class="select-member" :collection="$employees" selected="{{ $member->id }}" no-label start="0" label="{{ __('mission.employee') }}"></x-forms.select>
                            </td>
                            <td hidden>
                                <x-forms.select name="roles[]" class="form-control" :collection="$roles" selected="{{ $member->pivot->mission_role_id }}" no-label label="{{ __('mission.role') }}"></x-forms.select>
                            </td>
                            <td>
                                <x-forms.button class="btn-sm removeMissionMember btn-danger" data-id="'+trL+'"><i class="fas fa-minus-circle"></i></x-forms.button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </x-forms.control-container>
        </div><hr>
    </x-forms.base>

    <x-slot name="header">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </x-slot>

    <x-slot name="endbody">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        @include('missions.script.form')
    </x-slot>

    @push('modal')
    <livewire:transportation.form></livewire:transportation.form>
    @endpush

    @include('utils.livewire')
</x-layout>
