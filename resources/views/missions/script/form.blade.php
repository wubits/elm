<script>
    $('.select-member').select2();

    $('.addMissionMember').on('click',function(){

        var trL = (($('#missionMember tbody tr').length)+1);
        var index = $('#missionMember tbody tr:last').attr('id');
        var newIndex = parseInt(index.replace('rowMember_',''))+1;

        var members = '<select name="members[]" class="select-member form-control">'+
            '<option value="" selected >{{ __('employee.name') }}</option>'+
            @foreach ($employees as $employee)
                '<option value="{{ $employee->id }}" > {{ $employee->name }}</option>'+
            @endforeach
                '</select>';
        var tr = $('#missionMember tbody tr:first').clone().prop('id','rowMember_'+newIndex);
        tr.find('.member-no').html(trL);

        tr.find('td:nth(1)').html(members).find('select');
        $('#missionMember tbody').append(tr);
        $('.select-member').select2();

    });


    $(document).on("click", "#missionMember .removeMissionMember" , function() {
        var index = $(this).closest('tr').attr('id');
        removeMemberRow(index.replace('rowMember_',''));
        reOrderIndexMember();
    });

    function reOrderIndexMember()
    {
        var rowLeng =$('#missionMember tbody tr').length;
        var i=0;
        for(i=0; i<rowLeng; i++)
        {
            $('#missionMember tbody tr:nth('+i+') td:first').html(i+1);
        }
    };

    $(document).on("change", "#missionMember .missionMember" , function() {
        var index = $(this).closest('tr').attr('id');

        var positionId = $(this).val();
        var position='';
        $.ajax({
            type: 'GET',

            url: "{{ url('position/id') }}",
            data: { positionId: positionId },
            dataType: 'JSON',
            success: function (data) {
                //console.log(data.position.name);
                position += '<option >'+data.position.name+'</option>';
                // data.map(item =>{
                //   console.log(item);
                //   position += '<option value="'+item.id+'">'+item.name+'</option>';
                //   });
                $('#position-'+index.replace('rowMember_','')).html(position);


            },
        });
    });

    function removeMemberRow(rowId)
    {
        console.log(rowId);
        var rowLeng =$('#missionMember tbody tr').length;
        if(rowLeng==1){
            //alert("@lang('application.error_delectrow')");
            swal({
                title: "ព្រមាន",
                text: "អ្នកមិនអាចលុបទិន្នន័យទាំងអស់ទេ",
                icon: "warning",
                button: "យល់ព្រម",
            });
        }
        else{
            $('#rowMember_'+rowId).remove();
        }

    };

    $('.add-destination-row').on('click',function(){
        var trL = (($('#tb-distination tbody tr').length)+1);
        var index = $('#tb-distination tbody tr:last').attr('id');
        var newIndex = parseInt(index.replace('row_',''))+1;

        var tr = $('#tb-distination tbody tr:first').clone().prop('id','row_'+newIndex);
        tr.find(':selected').removeAttr('selected');
        var ods =  '<select  name="operational_districts[]" class="form-control data-od" id="od-'+newIndex+'" ><option value="" selected>{{ __('app.choose') }}{{ __('mission.operational_district') }}</option></select>';
        var facs = '<select  name="health_facilities[]" class="form-control data-hf" id="fac-'+newIndex+'" ><option value="" selected>{{ __('app.choose')  }}{{ __('mission.health_facility') }}</option></select>';

        tr.find('.destination-no').html(trL);
        tr.find('td:nth(2)').html(ods);
        tr.find('td:nth(3)').html(facs);
        $('#tb-distination tbody').append(tr);
    });


    $(document).on("click", "#tb-distination a.remove" , function() {
        var index = $(this).closest('tr').attr('id');
        removeRow(index.replace('row_',''));
        reOrderIndex();
    });

    function reOrderIndex()
    {
        var rowLeng =$('#tb-distination tbody tr').length;
        var i=0;
        for(i=0; i<rowLeng; i++)
        {
            $('#tb-distination tbody tr:nth('+i+') td:first').html(i+1);
        }
    }

    function removeRow(rowId)
    {
        var rowLeng =$('#tb-distination tbody tr').length;
        if(rowLeng==1){
            //alert("@lang('application.error_delectrow')");
            //swal("អ្នកមិនអាចលុបទិន្នន័យទាំងអស់ទេ");
            swal({
                title: "ព្រមាន",
                text: "អ្នកមិនអាចលុបទិន្នន័យទាំងអស់ទេ",
                icon: "warning",
                button: "យល់ព្រម",
            });
        }
        else{
            $('#tb-distination #row_'+rowId).remove();
        }

    };

    $(document).on("change", "#tb-distination .data-provinces" , function() {
        var index = $(this).closest('tr').attr('id');
        var proId = $(this).val();
        var ods = '';

        $.ajax({
            type: 'GET',
            url: "{{ url('province/ods') }}",
            data: { proId: proId },
            dataType: 'JSON',
            success: function (data) {
                ods += '<option value="">{{ __('app.choose') }}{{ __('mission.operational_district') }}</option>';
                data.map(item =>{
                    ods += '<option value="'+item.id+'">'+item.name+'</option>';
                });
                $('#od-'+index.replace('row_','')).html(ods);
            },
        });

    });

    $(document).on("change", "#tb-distination .data-od" , function() {
        var index = $(this).closest('tr').attr('id');
        var OpsId = $(this).val();
        var hfs = '';

        $.ajax({
            type: 'GET',
            url: "{{ url('province/facs') }}",
            data: { OpsId: OpsId },
            dataType: 'JSON',
            success: function (data) {
                hfs += '<option value="">{{ __('app.choose') }}{{ __('mission.health_facility') }}</option>';
                data.map(item =>{
                    hfs += '<option value="'+item.id+'">' + item.fullname + '</option>';
                });
                $('#fac-'+index.replace('row_','')).html(hfs);
            },
        });

    });
</script>
