<x-layout>
    <x-layouts.card>
        <x-slot name="title">
            <div class="row">
                <div class="col-4">
                    {{ __('app.mission') }} - {{ $mission->code }}
                </div>
                <div class="col-3">
                    {{ __('mission.leave_date') }} : {{ $mission->leave_date->diffForHumans() }}
                </div>
                <div class="col-3">
                    {{ __('mission.return_date') }} : {{ $mission->return_date->diffForHumans() }}
                </div>
                <div class="col-2">
                    <x-forms.button href="{{ route('mission.edit', $mission->id) }}">{{ __('app.edit') }}</x-forms.button>
                </div>
            </div>
{{--            @dump($mission->leave_date)--}}
        </x-slot>
        <div>

        </div>
    </x-layouts.card>
</x-layout>
