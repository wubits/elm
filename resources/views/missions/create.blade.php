<x-layout>
    <x-forms.base route="{{ route('mission.store') }}" enctype="multipart/form-data" backTo=" {{ route('mission.index') }}">
        <x-slot name="title">
            <h4>{{ __('mission.create') }}</h4>
        </x-slot>
        <div class="form-row mb-4">
            <div class="col-sm-3" >
                <x-forms.input name="code" label="{{ __('mission.code') }}"  ></x-forms.input>
            </div>
        </div>
        <x-forms.section>{{ __('app.general_information') }}</x-forms.section>
        <div class="form-row mb-4">
            <div class="col-sm-6">
                <x-forms.input name="reference_document" label="{{ __('mission.reference_document') }}" ></x-forms.input>
            </div>
            <div class="col-sm-3">
                <x-forms.date  name="leave_date" label="{{ __('mission.leave_date') }}"></x-forms.date>
            </div>
            <div class="col-sm-3">
                <x-forms.date name="return_date" label="{{ __('mission.return_date') }}"></x-forms.date>
            </div>
        </div>
        <div class="form-row mb-4">
            <div class="col">
                <x-forms.select name="objective_id" label="{{ __('mission.objective') }}" :collection="$objectives"></x-forms.select>
            </div>
            <div class="col">
                <livewire:transportation.select-box></livewire:transportation.select-box>
            </div>

            <div class="col">
                <x-forms.select name="budget_id" label="{{ __('mission.budget') }}" :collection="$budgets"></x-forms.select>
            </div>
        </div>
        <div class="form-row mb-4">
            <div class="col">
                <x-forms.textarea name="notes" label="{{ __('mission.note') }}"></x-forms.textarea>
            </div>

        </div>

        <x-forms.section>{{__('mission.destination')}}</x-forms.section>
        <div class="form-row mb-4">
            <x-forms.control-container class="col" name="provinces.*">
                <table class="table" id="tb-distination">
                    <thead>
                    <tr>
                        <x-tables.header>{{ __('#') }}</x-tables.header>
                        <x-tables.header class="w-25">{{ __('mission.province') }}</x-tables.header>
                        <x-tables.header class="w-25">{{ __('mission.operational_district') }}</x-tables.header>
                        <x-tables.header class="w-25">{{ __('mission.health_facility') }}</x-tables.header>
                        <x-tables.header><x-forms.button class="btn-sm add-destination-row"><i class="fas fa-plus-circle"></i></x-forms.button></x-tables.header>
                    </tr>
                    </thead>
                    <tbody>
                    <tr id="row_1">
                        <td class="destination-no" scope="row">1</td>
                        <td><x-forms.select name="provinces[]" class="data-provinces" :collection="$provinces" no-label label="{{ __('mission.province') }}"></x-forms.select></td>
                        <td><x-forms.select name="operational_districts[]" class="data-od" no-label id="od-1" label="{{ __('mission.operational_district') }}"></x-forms.select></td>
                        <td><x-forms.select name="health_facilities[]" class="data-hf" no-label id="fac-1" label="{{ __('mission.health_facility') }}"></x-forms.select></td>
                        <td><x-forms.button class="btn-sm remove btn-danger" data-id="'+trL+'"><i class="fas fa-minus-circle"></i></x-forms.button></td>
                    </tr>
                    </tbody>
                </table>
            </x-forms.control-container>
        </div><hr>

        <x-forms.section>{{ __('mission.member') }}</x-forms.section>


        <div class="form-row mb-4">
            <x-forms.control-container class="col" name="members.*">
            <table class="table" id="missionMember">
                    <thead>
                    <tr>
                        <x-tables.header>{{ __('#') }}</x-tables.header>
                        <x-tables.header class="w-75">{{ __('employee.name') }}</x-tables.header>
                        <x-tables.header hidden class="w-25">{{ __('mission.role') }}</x-tables.header>
                        <x-tables.header ><x-forms.button class="btn-sm addMissionMember"><i class="fas fa-plus-circle"></i></x-forms.button></x-tables.header>
                    </tr>
                    </thead>
                    <tbody>

                    <tr id="rowMember_1">
                        <td class="member-no" scope="row">1</td>
                        <td >
                            <x-forms.select name="members[]" class="select-member" :collection="$employees" no-label start="0" label="{{ __('mission.employee') }}"></x-forms.select>
                        </td>
                        <td hidden>
                            <x-forms.select  name="roles[]" class="form-control" :collection="$roles" no-label :selected="1" :value="1" label="{{ __('mission.role') }}"></x-forms.select>
                        </td>
                        <td>
                            <x-forms.button class="btn-sm removeMissionMember btn-danger" data-id="'+trL+'"><i class="fas fa-minus-circle"></i></x-forms.button>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </x-forms.control-container>
        </div><hr>
    </x-forms.base>

    <x-slot name="header">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </x-slot>

    <x-slot name="endbody">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        @include('missions.script.form')
    </x-slot>

    @push('modal')
        <livewire:transportation.form></livewire:transportation.form>
    @endpush

    @include('utils.livewire')
</x-layout>
