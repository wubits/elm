<x-forms.select noSelected name="transportation_id" label="{{ __('mission.transportation') }}" wire:model="transportation" :collection="$transportations" modal="transportation"></x-forms.select>
