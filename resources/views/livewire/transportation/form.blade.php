<x-livewire.modal>
    <x-slot name="title">
        {{ __('Create Transportation') }}
    </x-slot>
    <x-forms.input wire:model.lazy="transportation.name" name="name" label="{{ __('Transportation Name') }}"></x-forms.input>
    <x-forms.input wire:model.lazy="transportation.plate_number" name="plate_number" label="{{ __('Plate Number') }}"></x-forms.input>
</x-livewire.modal>
