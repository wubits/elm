<x-layout>
        <x-forms.base route="{{ route('roles.store') }}" backTo=" {{ route('roles.index') }}">
            <x-slot name="title">
                <h4>{{__('role.create​ role permission')}}</h4>
            </x-slot>
            <div class="row">
                <div class="col-12">
                    <x-forms.input
                        label="{{ __('role.name') }}"
                        name="name"
                        placeholder="{{ __('role.name') }}">
                    </x-forms.input>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12">
                    <x-forms.fieldset name="permissions">
                        <x-slot name="title">{{__('role.define_permission')}}<span class="text-danger">*</span></x-slot>
                        <div class="col-sm-3">
                            @foreach($permissions as $permission)
                                @if ($permission->group == "user")
                                    <x-forms.checkbox field="permissions[]" :selected="[]" :value="$permission->id">{{ $permission->name }}</x-forms.checkbox>
                                @endif
                            @endforeach
                        </div>
                        <div class="col-sm-3">
                            @foreach($permissions as $permission)
                                @if ($permission->group == "role")
                                    <x-forms.checkbox field="permissions[]" :selected="[]" :value="$permission->id">{{ $permission->name }}</x-forms.checkbox>
                                @endif
                            @endforeach
                        </div>
                        <div class="col-sm-3">
                            @foreach($permissions as $permission)
                                @if ($permission->group == "mission")
                                    <x-forms.checkbox field="permissions[]" :selected="[]" :value="$permission->id">{{ $permission->name }}</x-forms.checkbox>
                                @endif
                            @endforeach
                        </div>
                        <div class="col-sm-3">
                            @foreach($permissions as $permission)
                                @if ($permission->group == "employee")
                                    <x-forms.checkbox field="permissions[]" :selected="[]" :value="$permission->id">{{ $permission->name }}</x-forms.checkbox>
                                @endif
                            @endforeach
                        </div>
                    </x-forms.fieldset>
                </div>
            </div>
        </x-forms.base>
</x-layout>

