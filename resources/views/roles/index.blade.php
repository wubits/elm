<x-layout>
    <x-layouts.card>
        <x-slot name="title">
            <div class="float-left">
                <h4>{{__('role.create​ role permission')}}</h4>
            </div>
            <div class="float-right">
                @can('role-create')
                    <x-forms.button color="success" href="{{ route('roles.create') }}">{{__('role.create role')}}</x-forms.button>
                @endcan
            </div>
        </x-slot>
        <div class="col-12" style="margin-top:20px">
            <x-tables.base id="data-table_role">
                <x-slot name="header">
                    <x-tables.header>{{__('app.no')}}</x-tables.header>
                    <x-tables.header>{{__('app.role')}}</x-tables.header>
                    <x-tables.header>{{__('app.usage permission')}}</x-tables.header>
                    <x-tables.header style="width: 100px">{{__('app.action')}}</x-tables.header>
                </x-slot>
                @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                            @foreach($role->permissions as $permission)
                                <label class="badge badge-success">{{ $permission->name }}</label>
                            @endforeach
                        </td>
                        <td class="edit">
                            <x-tables.action
                                id="{{ $key }}"
                                editPermission="role-edit"
                                deletePermission="role-delete"
                                edit="{{ route('roles.edit', $role->id) }}"
                                destroy="{{ route('roles.destroy', $role->id) }}">
                            </x-tables.action>
                        </td>
                    </tr>
                @endforeach
            </x-tables.base>
        </div>
    </x-layouts.card>

    @push('styles')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    @endpush

</x-layout>

