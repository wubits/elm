<x-layout>

       <x-forms.base route="{{ route('users.profileUpdate', $user->id) }}" method="PATCH" enctype="multipart/form-data">
           <x-slot name="title">
               <div class="float-left">
                   <h4>{{__('app.user_profile')}}</h4>
               </div>
           </x-slot>
           <x-forms.input name="name" label="ឈ្មោះ" placeholder="ឈ្មោះ" value="{{ $user->name }}"></x-forms.input>
           <x-forms.input name="current_password" label="ពាក្យសំងាត់ចាស់" placeholder="ពាក្យសំងាត់ចាស់"></x-forms.input>
           <x-forms.input name="new_password" label="ពាក្យសំងាត់ថ្មី" placeholder="ពាក្យសំងាត់ថ្មី"></x-forms.input>
           <x-forms.input name="new_confirm_password" label="បញ្ជាក់ពាក្យសំងាត់ថ្មី" placeholder="បញ្ជាក់ពាក្យសំងាត់ថ្មី"></x-forms.input>
       </x-forms.base>


</x-layout>
