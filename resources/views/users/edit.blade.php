<x-layout>
    <x-forms.base route="{{route('users.update', $user->id)}}" method="patch" backTo="{{route('users.index')}}">
        <x-slot name="title">
             <h4>{{__('app.edit username')}}</h4>
        </x-slot>
        <div class="row">
            <div class="col-12">
                <x-forms.input 
                    label="{{ __('app.name') }}" 
                    name="name"
                    :value="$user->name"
                    placeholder="{{__('app.name') }}">  
                </x-forms.input>
            </div>
            <div class="col-12">
                <x-forms.input 
                    label="{{ __('app.email') }}" 
                    name="email"
                    :value="$user->email"
                    placeholder="{{__('app.email') }}">  
                </x-forms.input>
            </div>
            <div class="col-12">
                <x-forms.input 
                    label="{{ __('app.password') }}" 
                    name="password"
                    :value="$user->password"
                    type="password"
                    placeholder="{{__('app.password') }}">  
                </x-forms.input>
            </div>
            <div class="col-12">
                <x-forms.input 
                    label="{{ __('app.confirm_password') }}" 
                    name="confirm-password"
                    :value="$user->password"
                    type="password"
                    placeholder="{{__('app.confirm_password') }}">  
                </x-forms.input>
            </div>
            <div class="col-md-12">
                <x-forms.select 
                    label="{{ __('app.role')}}" 
                    name="roles[]"
                    :selecteds="$userRoles"
                    :collection="$roles"
                    noSelected
                    multiple>
                </x-forms.select>
            </div>
        </div>
    </x-forms.base>
</x-layout>
