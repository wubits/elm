<x-layout>
    <x-forms.base route="{{route('users.store')}}" backTo=" {{ route('users.index') }}">
        <div class="container">
            <div class="row mb-3">
                <x-slot name="title">
                    <h4>{{__('app.create new user')}}</h4>
                </x-slot>
            </div>
            <div class="user-create">
                <div class="row">
                    <div class="col-md-12">
                        <x-forms.input 
                            label="{{ __('app.name') }}" 
                            name="name"
                            placeholder="{{__('app.name') }}">  
                        </x-forms.input>
                    </div>
                    <div class="col-md-12">
                        <x-forms.input 
                            label="{{ __('app.email') }}" 
                            name="email"
                            placeholder="{{__('app.email') }}">  
                        </x-forms.input>
                    </div>
                    <div class="col-md-12">
                        <x-forms.input 
                            label="{{ __('app.password') }}" 
                            name="password"
                            type="password"
                            placeholder="{{__('app.password') }}"
                            >  
                        </x-forms.input>
                    </div>
                    <div class="col-md-12">
                        <x-forms.input 
                            label="{{ __('app.confirm_password') }}" 
                            name="confirm-password"
                            type="password"
                            placeholder="{{__('app.confirm_password') }}"
                            >  
                        </x-forms.input>
                    </div>
                    <div class="col-md-12">
                        <x-forms.select 
                            label="{{ __('app.role')}}" 
                            name="roles[]"
                            :collection="$roles"
                            multiple>
                        </x-forms.select>
                    </div>
                </div>
            </div>          
        </div>
    </x-forms.base>
</x-layout>
