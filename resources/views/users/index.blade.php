<x-layout>
    <x-layouts.card>
        <div class="container">
            <x-slot name="title">
                <div class="float-left">
                    <h4>{{__('app.create new username')}}</h4>
                </div>
                <div class="float-right">
                    @can('role-create')
                        <x-forms.button color="success" href="{{route('users.create')}}" style="font-size: 16px">{{__('app.create new user')}}
                        </x-forms.button>
                    @endcan
                </div>
            </x-slot>
            <div class="col-12" style="margin-top:20px">
                <x-tables.base id="data-table_user">
                    <x-slot name="header">
                        <x-tables.header>{{__('app.no')}}</x-tables.header>
                        <x-tables.header>{{__('app.name')}}</x-tables.header>
                        <x-tables.header>{{__('app.email')}}</x-tables.header>
                        <x-tables.header>{{__('role.display roles')}}</x-tables.header>
                        <x-tables.header>{{__('app.action')}}</x-tables.header>
                    </x-slot>
                    @foreach ($users as $index => $user)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if(!empty($user->getRoleNames()))
                                    @foreach($user->getRoleNames() as $role)
                                    <label class="badge badge-success">{{ $role }}</label>
                                    @endforeach
                                @endif
                            </td>
                            <td class="edit">
                                <x-tables.action
                                    id="{{ $index }}"
                                    editPermission="user-edit"
                                    deletePermission="user-delete"
                                    edit="{{route('users.edit', $user->id) }}"
                                    destroy="{{route('users.destroy', $user->id) }}">
                                </x-tables.action>
                            </td>
                        </tr>
                    @endforeach
                </x-tables.base>
            </div>
        </div>
    </x-layouts.card>
    <x-slot name="endbody">
        <script>
            $(document).ready( function (){
                $('#data-table_user').DataTable(
                    {
                        "language": {
                            url : "https://cdn.datatables.net/plug-ins/1.10.22/i18n/Khmer.json"
                        }
                    }
                );
            });
        </script>
    </x-slot>

     <x-slot name="header">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" defer></script>
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" />
    </x-slot>
</x-layout>
