<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Role And Permissions
Route::group(['middleware' => ['auth']], function() {

Route::get('users', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
Route::get('users/create/', [App\Http\Controllers\UserController::class, 'create'])->name('users.create');
Route::post('users/store/', [App\Http\Controllers\UserController::class, 'store'])->name('users.store');
Route::get('users/show/{id}', [App\Http\Controllers\UserController::class, 'show'])->name('users.show');
Route::get('users/edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit');
Route::patch('users/update/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');
Route::delete('users/destroy/{id}', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy');

Route::get('users/profile', [App\Http\Controllers\ProfileController::class, 'profile'])->name('users.profile');
Route::patch('users/profileUpdate/{id}', [App\Http\Controllers\ProfileController::class, 'profileUpdate'])->name('users.profileUpdate');

Route::get('roles',[App\Http\Controllers\RoleController::class, 'index'])->name('roles.index');
Route::get('roles/create',[App\Http\Controllers\RoleController::class, 'create'])->name('roles.create');
Route::post('roles/store',[App\Http\Controllers\RoleController::class, 'store'])->name('roles.store');
Route::get('roles/show/{id}', [App\Http\Controllers\RoleController::class, 'show'])->name('roles.show');
Route::get('roles/edit/{id}', [App\Http\Controllers\RoleController::class, 'edit'])->name('roles.edit');
Route::patch('roles/update/{id}', [App\Http\Controllers\RoleController::class, 'update'])->name('roles.update');
Route::delete('roles/destroy/{id}', [App\Http\Controllers\RoleController::class, 'destroy'])->name('roles.destroy');

//Employees
Route::resource('employees', App\Http\Controllers\EmployeeController::class);
Route::get('department/{depId}/positions', [App\Http\Controllers\DepartmentController::class, 'positions'])->name('department.positions');

});

//Mission
Route::group(['middleware' => ['auth']], function() {
Route::resource('mission',App\Http\Controllers\MissionController::class);
Route::get('mission/reback/{id}',[App\Http\Controllers\MissionController::class,'rebackDestroy'])->name('mission_back');

Route::get('province/ods',[App\Http\Controllers\MissionController::class,'getOdsByProId'])->name('province.ods');
Route::get('province/facs',[App\Http\Controllers\MissionController::class,'getFacByProId'])->name('province.facs');
Route::get('position/id/',[App\Http\Controllers\MissionController::class,'getPositionID'])->name('position.id');

});
